package com.sample.safzali.javabasicmondayssamplecode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BindingSampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_sample);
    }
}
